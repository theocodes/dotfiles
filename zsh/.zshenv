# misc

export PATH="$HOME/dotfiles/scripts:$HOME/.emacs.d/bin:$PATH"
export PATH="$PATH:$HOME/Code/brightcove/bolt-utils/bin"
export FABPATH="$HOME/Code/brightcove"

export EDITOR="nvim"
export ALTERNATE_EDITOR=""
export FZF_DEFAULT_COMMAND="find -L"
export GOSUMDB="off"

# python

export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"

# go

export GOENV_DISABLE_GOPATH=1
export GOPATH="$HOME/go"
export PATH="$PATH:$GOPATH/bin"
export PATH="$HOME/.goenv/bin:$PATH"

# ruby
export PATH="$HOME/.rbenv/bin:$PATH"

# node
export PATH="$HOME/.nodenv/bin:$PATH"

# deno
export DENO_INSTALL="/home/theocodes/.deno"
export PATH="$DENO_INSTALL/bin:$PATH"
