# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"
ZSH_THEME="robbyrussell"


plugins=(git zsh-autosuggestions zsh-syntax-highlighting)
source $ZSH/oh-my-zsh.sh

## aliases
alias ll='ls -lahG'
alias s='scripts'
alias k='kubectl'
alias vim='nvim'
alias capkey='od -a'
alias cp="cp -i"
alias df='df -h'
alias free='free -m'
alias e="$EDITOR"
alias t="tutils"

## init language envs
eval "$(rbenv init -)"
eval "$(pyenv init -)"
eval "$(nodenv init -)"
eval "$(goenv init -)"

## git
git config --global core.excludesfile ~/.gitignore
git config --global core.editor nvim

op-signin() {
  echo ">> authenticating secrets provider"
  eval $(secrets signin)
}

aws-setenv() {
    FILE="$HOME/.aws/cli/cache/$1.json"
    STS='{}'
    if [ -f "$FILE" ]; then
        STS=$(cat "$FILE")
        AWS_SESSION_TOKEN=$(echo "$STS" | jq -r '.Credentials.SessionToken // 1')
        export AWS_SESSION_TOKEN
    else
        unset AWS_SESSION_TOKEN
        echo "unset aws session token"
    fi
    AWS_ACCESS_KEY_ID=$(echo "$STS" | jq -r '.Credentials.AccessKeyId // 1')
    AWS_SECRET_ACCESS_KEY=$(echo "$STS" | jq -r '.Credentials.SecretAccessKey // 1')
    export AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY
    unset AWS_PROFILE
    unset AWS_DEFAULT_PROFILE
}

terraform-setenv() {
  op-signin
  echo ">> setting terraform env variables"

  export TF_VAR_bc_one_datadog_api_key=$(secrets get bc_one_datadog_api_key)
  export TF_VAR_bc_one_datadog_app_key=$(secrets get bc_one_datadog_app_key)
  export TF_VAR_bolt_datadog_api_key=$(secrets get bolt_datadog_api_key)
  export TF_VAR_bolt_datadog_app_key=$(secrets get bolt_datadog_app_key)
}

fastly-setenv() {
  op-signin
  echo ">> setting fastly env variables for: $1"

  if [[ ! -z "$1" ]]; then
    echo ">> setting fastly token for: $1"
    key="fastly_api_token_$1"
    export FASTLY_API_KEY=$(secrets get $key)
  else
    echo ">> FASTLY_API_KEY unset"
    unset FASTLY_API_KEY
  fi
}

bolt-setenv () {
  op-signin
  echo ">> setting env vars for bolt cli"

  export BOLT_AUTH_TOKEN="$(secrets get bolt_auth_token)"
  export FABRIC_MANAGEMENT_AUTH_TOKEN="$(secrets get fabric_management_auth_token)"
  # export FABRIC_PLAYBACK_AUTH_TOKEN="$(secrets get fabric_playback_auth_token)"
  export FABRIC_SOURCES_AUTH_TOKEN="$(secrets get fabric_sources_auth_token)"
  export GITHUB_TOKEN="$(secrets get github_token)"
}

circle-setenv () {
  op-signin
  echo ">> setting circle env variables for: $1"

  export BOLT_ENV=$1
  export AUTH_API_PRIVATE_KEY="$(secrets get circle_auth_api_private_key)"
  export CIRCLE_TOKEN="$(secrets get circle_token)"

  if [[ "$1" = "qa" ]]; then
    export ZENCODER_API_KEY_QA="$(secrets get zencoder_api_key_qa)"
    export AWS_ACCESS_KEY_ID_QA="$(secrets get circle_aws_access_key_id_qa)"
    export AWS_SECRET_ACCESS_KEY_QA="$(secrets get circle_aws_secret_access_key_qa)"
    export JWT_SIGNING_KEY_QA="$(secrets get circle_jwt_signing_qa_stage)"
  fi

  if [[ "$1" = "stage" ]]; then
    export ZENCODER_API_KEY_STAGE="$(secrets get zencoder_api_key_stage)"
    export AWS_ACCESS_KEY_ID_STAGE="$(secrets get circle_aws_access_key_id_stage)"
    export AWS_SECRET_ACCESS_KEY_STAGE="$(secrets get circle_aws_secret_access_key_stage)"
    export JWT_SIGNING_KEY_STAGE="$(secrets get circle_jwt_signing_qa_stage)"
  fi

  if [[ "$1" = "prod" ]]; then
    export ZENCODER_API_KEY_PROD="$(secrets get zencoder_api_key_prod)"
    export AWS_ACCESS_KEY_ID_PROD="$(secrets get circle_aws_access_key_id_prod)"
    export AWS_SECRET_ACCESS_KEY_PROD="$(secrets get circle_aws_secret_access_key_prod)"
    export JWT_SIGNING_KEY_PROD="$(secrets get circle_jwt_signing_prod)"
  fi
}

xinput set-prop 'pointer:Logitech G502' 'libinput Accel Speed' -0.7

fpath=( ~/dotfiles/zsh/.zshfn "${fpath[@]}" )
autoload -Uz $fpath[1]/*(.:t)

