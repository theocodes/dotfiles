if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
  silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
  Plug 'dyng/ctrlsf.vim'
  Plug 'junegunn/fzf.vim'
  Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'easymotion/vim-easymotion'
  Plug 'preservim/nerdtree'
  Plug 'tpope/vim-commentary'
  Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
  Plug 'vimwiki/vimwiki'
  Plug 'flazz/vim-colorschemes'
  Plug 'tpope/vim-fugitive'
  Plug 'rafi/awesome-vim-colorschemes'
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'liuchengxu/vim-which-key'
  Plug 'sheerun/vim-polyglot'
  Plug 'jiangmiao/auto-pairs'
  Plug 'caenrique/nvim-toggle-terminal'
  Plug 'mattn/emmet-vim'
call plug#end()

if has('termguicolors')
  set termguicolors
endif

colorscheme molokai
" colorscheme ayu

set t_Co=256
set clipboard+=unnamedplus  " use system clipboard
" set noshowmode              " don't show mode as airline already does
set hidden                  " enable hidden unsaved buffers
set expandtab               " insert tab with right amount of spacing
set tabstop=2               " use 2 spaces for tabs
set shiftwidth=2            " number of spaces to user for each step of auto-indent
set shiftround              " round indent to multiple of 'shiftwidth'
set mouse=a                 " enable mouse (selection, resizing etc)
set showcmd                 " show any commands

set history=200             " how many : commands to save in history
set nocompatible            " disable compatibility to old-time vi
set showmatch               " show matching brackets.

set ignorecase              " case insensitive matching
set hlsearch                " highlight search results
set autoindent              " indent a new line the same amount as the line just typed
set wildmode=longest,list   " get bash-like tab completions
set number relativenumber
filetype plugin indent on   " allows auto-indenting depending on file type
syntax on                   " syntax highlighting
set nobackup
set nowritebackup
set undofile " keep vim history
set undodir=~/.vimundo
set cursorline
set cursorcolumn

" autocompletion for vim commands
set wildmode=longest,list,full

" just gained another hour every 2 years
nnoremap ; :
vnoremap ; :

" transparent background
hi Normal guibg=None ctermbg=None
hi LineNr guibg=None
hi SignColumn guibg=None

autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o " no auto comment on new line

autocmd BufWritePre * :%s/\s\+$//e                              " remove trailing whitespace
" command W :execute ':silent w !sudo tee % > /dev/null' | :edit! " write with sudo

let g:mapleader="\<SPACE>" " space bar is leader
let g:maplocalleader = ',' " , is local leader

" disable match paren

let loaded_matchparen = 1

" open last buffer
nnoremap <silent> <Leader><Tab> :b#<CR>

" split
nnoremap <leader>h :split<Space>
nnoremap <leader>v :vsplit<Space>

" split navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" fix split
set splitbelow splitright

" center on insert
autocmd InsertEnter * norm zz

" replace all
nnoremap S :%s//gI<Left><Left><Left>

" search
nmap <leader>s :CtrlSF<Space>

" use Q to exit default register.
nnoremap q :q<CR>

" clear search highlights
if maparg('<C-C>', 'n') ==# ''
  nnoremap <silent> <C-C> :nohlsearch<CR><C-C>
endif

" go-vim
let g:go_fmt_command = "goimports"
let g:go_def_mode='gopls'
let g:go_info_mode='gopls'

" coc

set updatetime=300
set cmdheight=2
set shortmess+=c

command! -nargs=0 Prettier :CocCommand prettier.formatFile
let g:user_emmet_leader_key='<C-F>'

" use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" goto code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocActionAsync('doHover')
  endif
endfunction

" use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

" up and down completion with C-[jk]
inoremap <expr> <C-j> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <C-k> pumvisible() ? "\<C-p>" : "\<S-Tab>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" toggle nerdtree
nmap <silent> <C-o> :NERDTreeToggle<CR>

let g:NERDTreeIgnore = ['^node_modules$']

" toggle comment
nmap <C-_> <Plug>CommentaryLine<CR>
vmap <C-_> <Plug>Commentary<CR>
nmap <Leader>cm <Plug>CommentaryLine<CR>
vmap <Leader>cm <Plug>Commentary<CR>

" vimwiki

let g:vimwiki_list = [{'path': '~/journal/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]

" files
nnoremap <silent> <C-p> :Files<CR>
nnoremap <silent> <C-s> :Rg<CR>
nnoremap <silent> <C-l> :Lines<CR>

" project
nnoremap <silent> <leader>pf :GFiles<CR>

" git
nnoremap <silent> <leader>gs :GFiles?<CR>
nnoremap <silent> <leader>gc :Commits<CR>

" buffers
nnoremap <silent> <C-b> :Buffers<CR>
nnoremap <silent> <leader>bd :bd<CR> :bprevious<CR>

" config management
map <leader>conf :e ~/.config/nvim/init.vim<CR>
map <leader>load :source %<CR>
map <leader>install :PlugInstall<CR>
map <leader>clean :PlugClean<CR>
map <leader>zshrc :e ~/.zshrc<CR>
map <leader>zshenv :e ~/.zshenv<CR>

" terminal
nnoremap <silent> <C-z> :ToggleTerminal<CR>
tnoremap <silent> <C-z> <C-\><C-n>:ToggleTerminal<CR>

" easymotion
nmap <silent> s <Plug>(easymotion-overwin-f)

nmap <silent> <leader>ji :VimwikiIndex<CR>
nmap <silent> <leader>jn :VimwikiMakeDiaryNote<CR>
nmap <silent> <leader>jt :VimwikiMakeTomorrowDiaryNote<CR>
